package com.example.macbookpro.domicilioscom.base;

import android.app.ProgressDialog;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.view.KeyEvent;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.example.macbookpro.domicilioscom.R;
import com.example.macbookpro.domicilioscom.domain.InjectComponents.DaggerSchoolComponentsViews;
import com.example.macbookpro.domicilioscom.domain.InjectComponents.SchoolApp;
import com.example.macbookpro.domicilioscom.domain.InjectComponents.SchoolComponentsViews;
import com.example.macbookpro.domicilioscom.domain.InjectComponents.SchoolViews;

public abstract class baseFragment extends Fragment {

    private ProgressDialog mProgress;

    protected abstract int getFragmentLayout();
    protected abstract void controlsInit();

    public MainActivity MyActivity( ){
        return (MainActivity) getActivity();
    }

    public SchoolComponentsViews dependencyInjection(PrincipalView pv ){ // Inyecta dependencias de todas las vistas
       return DaggerSchoolComponentsViews
                .builder()
                .schoolComponents(SchoolApp.getApp(this.getContext( )).getDaggerCmp( ))
                .schoolViews(new SchoolViews(pv))
                .build();
    }

    protected Fragment getFragment(){
        return this;
    };

    protected void hideProgres(){ // Esconde el progreso
        if (mProgress != null){
            if (mProgress.isShowing()){
                mProgress.dismiss();
            }
        }
    }

    protected void showProgress(String titulo, String msg){ // Activa el progreso
        if (mProgress != null){
            if (mProgress.isShowing()){
                mProgress.dismiss();
            }
        }

        if(getContext() != null) {
            Drawable drawable = new ProgressBar(getContext()).getIndeterminateDrawable().mutate();
            drawable.setColorFilter(ContextCompat.getColor(getContext(), R.color.colorAccent), PorterDuff.Mode.SRC_IN);

            mProgress = new ProgressDialog(getContext());
            mProgress.setTitle(titulo);
            mProgress.setMessage(Html.fromHtml(msg));
            mProgress.setCancelable(false);
            mProgress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgress.setIndeterminateDrawable(drawable);
            mProgress.show();
        }
    }

    protected void showMessage(int msg){
        if(getContext() != null)
        Toast.makeText(getContext(), msg ,Toast.LENGTH_SHORT).show();
    }

    protected void showMessage(String msg){
        if(getContext() != null)
        Toast.makeText(getContext(),msg,Toast.LENGTH_SHORT).show();
    }

    protected void backBtnFuction( BackBtnListener backBtnListener ){ // Setea el listener cuando el usuario oprime back fisico
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener((v, keyCode, event) -> {
            if( keyCode == KeyEvent.KEYCODE_BACK )  {
                backBtnListener.onClickButtonPresed( );
                return true;
            }
            return false;
        });
    }


    public interface DialogListener{
        void onClickButtonPresed();
    }

    public interface BackBtnListener{
        void onClickButtonPresed();
    }
}
