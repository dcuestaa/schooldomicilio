package com.example.macbookpro.domicilioscom.domain.InjectComponents;

import com.example.macbookpro.domicilioscom.MVP.SplashFragment.View.splashView;
import com.example.macbookpro.domicilioscom.base.PrincipalView;
import dagger.Module;
import dagger.Provides;

/**
 * Created by macbookpro on 15/05/18.
 */

@Module
public class SchoolViews {
    private final PrincipalView view;

    public SchoolViews(PrincipalView view) { this.view = view; }

    @Provides public splashView provideSplashView() { return (splashView) this.view; }
}
