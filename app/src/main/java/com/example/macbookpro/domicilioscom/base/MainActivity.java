package com.example.macbookpro.domicilioscom.base;

import android.os.Bundle;
import android.support.design.widget.BottomNavigationView;
import com.example.macbookpro.domicilioscom.MVP.SplashFragment.View.splashViewImp;
import com.example.macbookpro.domicilioscom.R;

public class MainActivity extends baseActivity implements PrincipalView {


    private static MainActivity isntanceActivity;
    public static MainActivity getActivityMain( ){
        return isntanceActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        isntanceActivity = this; // Asgina la primera instancia como estatica y unica
        InitComponents();
    }

    private void InitComponents(){
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        NavigationFragment(splashViewImp.newInstance(), splashViewImp.TAG_SPLASH, false); // Abre la pantalla de SPLASH
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = item -> {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        //mTextMessage.setText(R.string.title_home);
                        return true;
                    case R.id.navigation_dashboard:
                       // mTextMessage.setText(R.string.title_dashboard);
                        return true;
                    case R.id.navigation_notifications:
                        //mTextMessage.setText(R.string.title_notifications);
                        return true;
                }
        return false;
    };


    @Override
    public void showMessage(int idError) {

    }

    @Override
    public void showMessage(String error) {

    }

    @Override
    public void showProgress(String titulo, String mensaje) {

    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void backBtnFuction(baseFragment.BackBtnListener backBtnListener) {

    }
}
