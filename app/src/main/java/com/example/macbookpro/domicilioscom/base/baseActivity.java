package com.example.macbookpro.domicilioscom.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.example.macbookpro.domicilioscom.R;

/**
 * Created by macbookpro on 15/05/18.
 */

public class baseActivity extends AppCompatActivity  {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // Cmabia de fragmento( vista )
    protected void NavigationFragment(Fragment fragment, String tagFragment, boolean back){
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if (back)
            ft.setCustomAnimations(R.anim.right_in, R.anim.right_out);
        else
            ft.setCustomAnimations(R.anim.left_in, R.anim.left_out);
        ft.replace(R.id.content_frame, fragment, tagFragment);
        ft.isAddToBackStackAllowed();
        ft.commit();
    }
}
