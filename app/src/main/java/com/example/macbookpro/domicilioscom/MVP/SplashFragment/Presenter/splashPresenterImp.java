package com.example.macbookpro.domicilioscom.MVP.SplashFragment.Presenter;

import android.os.CountDownTimer;
import android.widget.ImageView;
import com.example.macbookpro.domicilioscom.MVP.SplashFragment.Model.splashInteractorImp;
import com.example.macbookpro.domicilioscom.MVP.SplashFragment.View.splashView;
import com.example.macbookpro.domicilioscom.base.MainActivity;
import com.example.macbookpro.domicilioscom.R;
import com.example.macbookpro.domicilioscom.base.basePresenter;
import javax.inject.Inject;

/**
 * Created by Dcuesta on 15/05/2018.
 */

public class splashPresenterImp extends basePresenter {

    private splashView view;
    private splashInteractorImp interactor;

    int idxImg;
    int countG;

    Integer [] listImgs = new Integer[]{
            R.drawable.splash_bus};


    @Inject
    public splashPresenterImp(splashView view, splashInteractorImp interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    public void setImageSplash(ImageView view ){

        idxImg = 0;
        countG = 0;

        CountDownTimer countDownTimer = new CountDownTimer(1000,180) {
            @Override
            public void onTick(long millisUntilFinished) {
                view.setImageDrawable(MainActivity.getActivityMain().getResources().getDrawable(listImgs[idxImg]));
                if( idxImg < listImgs.length - 1)
                    idxImg++;
                else{
                    countG++;
                    idxImg =0;
                }
            }

            @Override
            public void onFinish() {

            }
        };
        countDownTimer.start();
    }

}
