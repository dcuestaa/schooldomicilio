package com.example.macbookpro.domicilioscom.base;

/**
 * Created by Dcuesta on 15/05/2018.
 */

public interface PrincipalView {
        void showMessage(int idError);
        void showMessage(String error);
        void showProgress(String titulo, String mensaje);
        void hideProgress();
        void backBtnFuction(baseFragment.BackBtnListener backBtnListener);
}

