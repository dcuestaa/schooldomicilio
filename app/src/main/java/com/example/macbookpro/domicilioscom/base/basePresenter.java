package com.example.macbookpro.domicilioscom.base;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Dcuesta on 15/05/18.
 */

public class basePresenter {

    protected void HideKeyBoard( ){ // Desactiva el teclado mobile en caso de que este activado
        try{InputMethodManager inputManager = (InputMethodManager) MainActivity.getActivityMain().getSystemService(Context.INPUT_METHOD_SERVICE );
            inputManager.hideSoftInputFromWindow(MainActivity.getActivityMain().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }catch (Exception e){}
    }

    protected boolean isConnected() { // Valida si hay conexion a internet via wifi o datos
        ConnectivityManager connectivityManager
                = (ConnectivityManager) MainActivity.getActivityMain( ).getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
