package com.example.macbookpro.domicilioscom.MVP.SplashFragment.View;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.example.macbookpro.domicilioscom.MVP.SplashFragment.Presenter.splashPresenterImp;
import com.example.macbookpro.domicilioscom.R;
import com.example.macbookpro.domicilioscom.base.baseFragment;

import javax.inject.Inject;

/**
 * Created by macbookpro on 15/05/18.
 */


@SuppressLint("ValidFragment")
public class splashViewImp extends baseFragment implements splashView {

    public static final String TAG_SPLASH = "FG_SPLASH";

    @Inject
    protected splashPresenterImp presenter;
    private View viewFragment;
    ImageView iconSplasImg;

    @Override
    protected int getFragmentLayout() {
        return R.layout.splash_fragment;
    }

    public static Fragment newInstance() {
        Fragment fragment = new splashViewImp();
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        controlsInit();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return  viewFragment = inflater.inflate(R.layout.splash_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dependencyInjection(this).inject(this); // crea la inyeccion de dependencias de mi vista
    }

    @Override
    protected void controlsInit() {
        iconSplasImg = viewFragment.findViewById(R.id.iconSplasImg);
        presenter.setImageSplash(iconSplasImg);
    }

    @Override
    public void showMessage(int idError) {
        super.showMessage(idError);
    }

    @Override
    public void showMessage(String error) {
        super.showMessage(error);
    }

    @Override
    public void showProgress(String titulo, String mensaje) {
        super.showProgress(titulo, mensaje);
    }

    @Override
    public void hideProgress() {
        super.hideProgres();
    }

    @Override
    public void backBtnFuction(BackBtnListener backBtnListener) {
        super.backBtnFuction(backBtnListener);
    }
}
