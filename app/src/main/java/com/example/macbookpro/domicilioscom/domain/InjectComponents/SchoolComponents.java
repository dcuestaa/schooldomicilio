package com.example.macbookpro.domicilioscom.domain.InjectComponents;

import com.example.macbookpro.domicilioscom.base.baseActivity;
import com.example.macbookpro.domicilioscom.domain.Io.SchoolApiServices;
import javax.inject.Singleton;
import dagger.Component;

/**
 * Created by macbookpro on 15/05/18.
 */


@Singleton
@Component(modules = SchoolModule.class)
interface SchoolComponents {
    void inject(SchoolApp experianApp);
    void inject(baseActivity mainActivity);
    SchoolApiServices getSchoolApiService();
}
