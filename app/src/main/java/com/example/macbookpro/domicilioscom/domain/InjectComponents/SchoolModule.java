package com.example.macbookpro.domicilioscom.domain.InjectComponents;

import android.app.Application;
import android.content.Context;

import com.example.macbookpro.domicilioscom.domain.Io.SchoolApiAdapter;
import com.example.macbookpro.domicilioscom.domain.Io.SchoolApiServices;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

/**
 * Created by macbookpro on 15/05/18.
 */


@Module
public class SchoolModule {

        private final Application application;
        public SchoolModule(Application app) { this.application = app; }

        @Provides
        public Context provideContext(){ return application; }

        @Provides
        public Retrofit provideRetrofitInstance(){ return SchoolApiAdapter.getInstance( ); }

        @Provides
        public SchoolApiServices provideReconoSerApiService(Retrofit retrofit ){
            return retrofit.create(SchoolApiServices.class);
        }
}
