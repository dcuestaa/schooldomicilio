package com.example.macbookpro.domicilioscom.domain.InjectComponents;

import android.app.Application;
import android.content.Context;

/**
 * Created by macbookpro on 15/05/18.
 */

public class SchoolApp extends Application{
    private SchoolComponents experianComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        getDaggerCmp();
    }

    public SchoolComponents getDaggerCmp() {
        if (experianComponent == null) {
            experianComponent = DaggerSchoolComponents
                    .builder()
                    .schoolModule(new SchoolModule(this))
                    .build();

            experianComponent.inject(this);
        }
        return experianComponent;
    }

    public static SchoolApp getApp (Context context){
        return (SchoolApp) context.getApplicationContext();
    }

}
