package com.example.macbookpro.domicilioscom.domain.InjectComponents;

import com.example.macbookpro.domicilioscom.MVP.SplashFragment.View.splashViewImp;
import com.example.macbookpro.domicilioscom.base.baseActivity;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import javax.inject.Scope;
import dagger.Component;

/**
 * Created by macbookpro on 15/05/18.
 */

@ftScope
@Component(dependencies = SchoolComponents.class, modules = SchoolViews.class)
public interface SchoolComponentsViews {
    void inject(baseActivity activity);
    void inject(splashViewImp fragment);
}

@Scope
@Retention(RetentionPolicy.RUNTIME)
@interface ftScope{

}
